#Author: Imteaz Siddique
#Scope: The purpose of this script is to parse through multiple mRNA library metrics summary and generate a table

from pathlib import Path
from pathlib import PurePath
import argparse
import pandas as pd
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('-f', type=Path, help='add file path to metrics summary')
parser.add_argument('-o', type=str, help='add name of output csv file')
parser.add_argument('-c', type=int, help='add how many cells used in experiment')
path = parser.parse_args().f
labelOutput = parser.parse_args().o
cellNumber = parser.parse_args().c
dirFile = list(Path(path).glob('*_Metrics_Summary.csv'))

dfIndex = ['Total reads', '%reads filtered out', '%Q30', '%read assigned cell label', '%cellular read aligned to amplicon', 
           'Raw seq depth', 'Mean RSEC seq depth', 'Mean DBEC seq depth', 'Seq saturation', '# of cells called',
           'Expected # of cells', '%RSEC reads from putative cells', 'Mean RSEC reads per cell', 'Mean RSEC mols per cell',
           'Mean RSEC genes per cell', '%DBEC reads from putative cells', 'Mean DBEC reads per cell', 'Mean DBEC mols per cell',
           'Mean DBEC genes per cell', '# of under sequenced genes']

workdf = pd.DataFrame(index=dfIndex)
#workdf['new']=np.nan
#print(workdf)
lineCount = 0
for files in dirFile:
    name = files.name
    workdf[name]=np.nan
    fh = open(files)
    
    for lines in fh:
        lines = lines.split(',')
        if lineCount == 8:
            workdf[name]['Total reads'] = lines[0]
            workdf[name]['%reads filtered out'] = lines[4]
            
        elif lineCount == 12:
            workdf[name]['%Q30'] = lines[2]
            workdf[name]['%read assigned cell label'] = lines[3]
            workdf[name]['%cellular read aligned to amplicon'] = lines[4]
        
        elif lineCount == 16:
            workdf[name]['Raw seq depth'] = lines[4]
            workdf[name]['Mean RSEC seq depth'] = lines[5]
            workdf[name]['Mean DBEC seq depth'] = lines[6]
            workdf[name]['Seq saturation'] = lines[7]
        
        elif lineCount == 20:
            workdf[name]['# of cells called'] = lines[0]
            workdf[name]['Expected # of cells'] = cellNumber
            workdf[name]['%RSEC reads from putative cells'] = lines[1]
            workdf[name]['Mean RSEC reads per cell'] = lines[2]
            workdf[name]['Mean RSEC mols per cell'] = lines[3]
            workdf[name]['Mean RSEC genes per cell'] = lines[5]
            
        elif lineCount == 24:
            workdf[name]['%DBEC reads from putative cells'] = lines[1]
            workdf[name]['Mean DBEC reads per cell'] = lines[2]
            workdf[name]['Mean DBEC mols per cell'] = lines[3]
            workdf[name]['Mean DBEC genes per cell'] = lines[5]
            
        elif lineCount == 28:
            workdf[name]['# of under sequenced genes'] = lines[1]
            
        lineCount = lineCount + 1
    lineCount = 0
    fh.close()
resultPath = PurePath(path).joinpath(labelOutput)
workdf.to_csv(resultPath)
