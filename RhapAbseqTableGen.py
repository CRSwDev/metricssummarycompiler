#Author: Imteaz Siddique
#Scope: Parses through the abseq metrics summary files
import argparse
from pathlib import Path
from pathlib import PurePath
import pandas as pd
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('-f', type=Path)
parser.add_argument('-o', type=str)
labelOutput = parser.parse_args().o
path = parser.parse_args().f
dirFile = list(Path(path).glob('*_Metrics_Summary.csv'))

dfIndex = ['Total reads', '%reads filtered out', '%Q30', '%read assigned cell label', '%cellular read aligned to amplicon', 
           'mRNA Raw seq depth', 'mRNA Mean RSEC seq depth', 'mRNA Mean DBEC seq depth', 'mRNA Seq saturation', 
           'AbSeq Raw seq depth', 'AbSeq Mean RSEC seq depth', 'AbSeq Mean DBEC seq depth', 'AbSeq Seq saturation','# of cells called',
           '%RSEC reads from putative cells', 'Mean RSEC reads per cell', 'Mean RSEC mols per cell', 'Mean RSEC genes per cell',
           '# of cells called abseq', '%RSEC reads from putative cells abseq', 'Mean RSEC reads per cell abseq',
           'Mean RSEC mols per cell abseq', 'Mean RSEC genes per cell abseq']

workdf = pd.DataFrame(index=dfIndex)
#workdf['new']=np.nan
#print(workdf)
lineCount = 0
for files in dirFile:
    workdf[files]=np.nan
    fh = open(files)
    
    for lines in fh:
        lines = lines.split(',')
        if lineCount == 10:
            workdf[files]['Total reads'] = lines[0]
            workdf[files]['%reads filtered out'] = lines[4]
            
        elif lineCount == 16:
            workdf[files]['%Q30'] = lines[2]
            workdf[files]['%read assigned cell label'] = lines[3]
            workdf[files]['%cellular read aligned to amplicon'] = lines[4]
        
        elif lineCount == 20:
            workdf[files]['mRNA Raw seq depth'] = lines[4]
            workdf[files]['mRNA Mean RSEC seq depth'] = lines[5]
            workdf[files]['mRNA Mean DBEC seq depth'] = lines[6]
            workdf[files]['mRNA Seq saturation'] = lines[7]
        
        elif lineCount == 21:
            workdf[files]['AbSeq Raw seq depth'] = lines[4]
            workdf[files]['AbSeq Mean RSEC seq depth'] = lines[5]
            workdf[files]['AbSeq Mean DBEC seq depth'] = lines[6]
            workdf[files]['AbSeq Seq saturation'] = lines[7]
        
        elif lineCount == 26:
            workdf[files]['# of cells called'] = lines[0]
            workdf[files]['%RSEC reads from putative cells'] = lines[1]
            workdf[files]['Mean RSEC reads per cell'] = lines[2]
            workdf[files]['Mean RSEC mols per cell'] = lines[3]
            workdf[files]['Mean RSEC genes per cell'] = lines[5]

        elif lineCount == 27:
            workdf[files]['# of cells called abseq'] = lines[0]
            workdf[files]['%RSEC reads from putative cells abseq'] = lines[1]
            workdf[files]['Mean RSEC reads per cell abseq'] = lines[2]
            workdf[files]['Mean RSEC mols per cell abseq'] = lines[3]
            workdf[files]['Mean RSEC genes per cell abseq'] = lines[5]
            
        lineCount = lineCount + 1
    lineCount = 0
    fh.close()
resultPath = PurePath(path).joinpath(labelOutput)
workdf.to_csv(resultPath)