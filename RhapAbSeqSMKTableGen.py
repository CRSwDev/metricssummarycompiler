# Author: Imteaz Siddique
# Scope: Parses through the abseq metrics summary files
from pathlib import Path
from pathlib import PurePath
import pandas as pd
import numpy as np

path = input('Enter file path: ')
labelOutput = input("name of output file: ")
STlabelOutput = input("name of Sample Tag output file: ")
dirFile = list(Path(path).glob('*_Metrics_Summary.csv'))
smkFile = list(Path(path).glob('*_Sample_Tag_Metrics.csv'))

dfIndex = ['Total reads', '%reads filtered out', '%Q30', '%read assigned cell label',
           '%cellular read aligned to amplicon',
           'mRNA Raw seq depth', 'mRNA Mean RSEC seq depth', 'mRNA Mean DBEC seq depth', 'mRNA Seq saturation',
           'AbSeq Raw seq depth', 'AbSeq Mean RSEC seq depth', 'AbSeq Mean DBEC seq depth', 'AbSeq Seq saturation',
           '# of cells called',
           '%RSEC reads from putative cells', 'Mean RSEC reads per cell', 'Mean RSEC mols per cell',
           'Mean RSEC genes per cell',
           '# of cells called abseq', '%RSEC reads from putative cells abseq', 'Mean RSEC reads per cell abseq',
           'Mean RSEC mols per cell abseq', 'Mean RSEC genes per cell abseq']

'''Creating Index for Sample Tag File'''
SMKIndex = []
for i in range(12):
    spec1 = 'ST' + str(i + 1) + ' Raw Reads'
    spec2 = 'ST' + str(i + 1) + ' Cells Called'
    spec3 = 'ST' + str(i + 1) + ' % Putative Cells Called'
    spec4 = 'ST' + str(i + 1) + ' mean reads per called cell'
    tempList = [spec1, spec2, spec3, spec4]
    SMKIndex = SMKIndex + tempList
SMKIndex = SMKIndex + ['mult Raw Reads', 'mult Cells Called', 'mult % Putative Cells Called',
                       'mult mean reads per called cell']
workdf = pd.DataFrame(index=dfIndex)
smkdf = pd.DataFrame(index=SMKIndex)
# workdf['new']=np.nan
# print(workdf)
lineCount = 0
for files in dirFile:
    workdf[files] = np.nan
    fh = open(files)

    for lines in fh:
        lines = lines.split(',')
        if lineCount == 12:
            workdf[files]['Total reads'] = lines[0]
            workdf[files]['%reads filtered out'] = lines[4]

        elif lineCount == 19:
            workdf[files]['%Q30'] = lines[2]
            workdf[files]['%read assigned cell label'] = lines[3]
            workdf[files]['%cellular read aligned to amplicon'] = lines[4]

        elif lineCount == 23:
            workdf[files]['mRNA Raw seq depth'] = lines[4]
            workdf[files]['mRNA Mean RSEC seq depth'] = lines[5]
            workdf[files]['mRNA Mean DBEC seq depth'] = lines[6]
            workdf[files]['mRNA Seq saturation'] = lines[7]

        elif lineCount == 24:
            workdf[files]['AbSeq Raw seq depth'] = lines[4]
            workdf[files]['AbSeq Mean RSEC seq depth'] = lines[5]
            workdf[files]['AbSeq Mean DBEC seq depth'] = lines[6]
            workdf[files]['AbSeq Seq saturation'] = lines[7]

        elif lineCount == 29:
            workdf[files]['# of cells called'] = lines[0]
            workdf[files]['%RSEC reads from putative cells'] = lines[1]
            workdf[files]['Mean RSEC reads per cell'] = lines[2]
            workdf[files]['Mean RSEC mols per cell'] = lines[3]
            workdf[files]['Mean RSEC genes per cell'] = lines[5]

        elif lineCount == 30:
            workdf[files]['# of cells called abseq'] = lines[0]
            workdf[files]['%RSEC reads from putative cells abseq'] = lines[1]
            workdf[files]['Mean RSEC reads per cell abseq'] = lines[2]
            workdf[files]['Mean RSEC mols per cell abseq'] = lines[3]
            workdf[files]['Mean RSEC genes per cell abseq'] = lines[5]

        lineCount = lineCount + 1
    lineCount = 0
    fh.close()
resultPath = PurePath(path).joinpath(labelOutput)
workdf.to_csv(resultPath)

smkCount = 9
stCount = 1
for item in smkFile:
    smkdf[item] = np.nan
    fh = open(item)

    for lines in fh:
        lineSplit = lines.split(',')
        if lineCount == 21:
            smkdf[item]['mult Raw Reads'] = lineSplit[2]
            smkdf[item]['mult Cells Called'] = lineSplit[4]
            smkdf[item]['mult % Putative Cells Called'] = lineSplit[5]
            smkdf[item]['mult mean reads per called cell'] = lineSplit[7]

        elif lineCount == smkCount:
            sampleTag = 'ST' + str(stCount)
            print(sampleTag)
            smkdf[item][sampleTag + ' Raw Reads'] = lineSplit[2]
            smkdf[item][sampleTag + ' Cells Called'] = lineSplit[4]
            smkdf[item][sampleTag + ' % Putative Cells Called'] = lineSplit[5]
            smkdf[item][sampleTag + ' mean reads per called cell'] = lineSplit[7]
            smkCount = smkCount + 1
            stCount = stCount + 1

        lineCount = lineCount + 1
    lineCount = 0
    smkCount = 9
    stCount = 1

print(smkdf)
resultPath = PurePath(path).joinpath(STlabelOutput)
smkdf.to_csv(resultPath)
