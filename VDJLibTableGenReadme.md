# VDJ Library Table Generator

This script combines metrics from different VDJ sequencing libraries\
that have gone through the VDJ pipeline.

## Installation

None Required. Just copy the script to a directory of your choice.

## Dependencies
```bash
python 3.7.1
numpy 1.16.2
pandas 0.24.1
```

## Usage
Copy script to a directory of your choice.\
Compile all metrics summaries into one folder.\
Be sure that all metrics summaries are from the VDJ pipeline.\
Run script.
```bash
python VDJLibTableGen.py
```
Use absolute path to indicate where your summaries are saved.\
Input your output name. example: "something.csv" \

## Contributing
Please reach out to me through slack or email to suggest improvements.