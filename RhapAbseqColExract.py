#Author: Imteaz Siddique
#Scope: Loop through data tables in a folder and then recreate a new folder with new files containing only the
#AbSeq columns

import os
import argparse
import pandas as pd
from pathlib import Path

def run_prog(path):
    file_list = list(path.glob("*_MolsPerCell.csv"))
    resultpath = path.joinpath('result')
    os.mkdir(resultpath)
    for items in file_list:
        name = items.name
        tempList = list()
        run_df = pd.read_csv(items, index_col=0, comment='#')
        colList = run_df.columns.values.tolist()
        for item in colList:
            if 'pAbO' in item:
                tempList.append(item)
        result_df = run_df[tempList]
        savePath = resultpath.joinpath(name)
        result_df.to_csv(savePath)
        print(items, "done")

def main():
    parser = argparse.ArgumentParser(description="Goes through files in a folder and extracts abseq columns of "
                                                 "each file.")
    parser.add_argument("-dir", type=Path, help="Path to where files are stored")
    filePath = parser.parse_args().dir
    run_prog(filePath)

if __name__ == "__main__":
    main()