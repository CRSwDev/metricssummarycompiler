#Author: Imteaz Siddique
#Scope: The purpose of this script is to parse through multiple VDJ pipeline metrics summaries and compile them into one csv file.

from pathlib import Path
from pathlib import PurePath
import pandas as pd
import numpy as np

path = input('Enter file path: ')
dirFile = list(Path(path).glob('*_PipelineMetrics.csv'))

dfIndexTCR = ['TCR total reads', 'TCR %reads w/linker', 'TCR %reads to CL', 'TCRa %read2 aligned',
              'TCRb %read2 aligned', 'TCR %read2 sum', 'TCRa %read CDR3', 'TCRb %read CDR3', 'TCRa %read w/constant',
              'TCRb %read w/constant', 'TCRa %aligned w/ cellular reads', 'TCRb %aligned w/ cellular reads',
              '%TCRa RSEC putative reads', '%TCRb RSEC putative reads', 'TCRa Total molecules', 'TCRb Total molecules',
              'TCRa RSEC Depth', 'TCRb RSEC Depth', 'TCRa DBEC Depth', 'TCRb DBEC Depth', 'TCR %useful reads']

#Edit this list for the BCR metrics
dfIndexBCR = ['BCR total reads', 'BCR %reads w/linker', 'BCR %reads to CL', 'IgH %read2 aligned',
              'IgL %read2 aligned', 'BCR %read2 sum', 'IgH %read CDR3', 'IgL %read CDR3', 'IgH %read w/constant',
              'IgL %read w/constant', 'IgH %aligned w/ cellular reads', 'IgL %aligned w/ cellular reads',
              '%IgH RSEC putative reads', '%IgL RSEC putative reads', 'IgH Total molecules', 'IgL Total molecules',
              'IgH RSEC Depth', 'IgL RSEC Depth', 'IgH DBEC Depth', 'IgL DBEC Depth', 'BCR %useful reads']

TCRdf = pd.DataFrame(index=dfIndexTCR)
BCRdf = pd.DataFrame(index=dfIndexBCR)
linecount = 0
for files in dirFile:

    name = files.name
    TCRdf[name] = np.nan
    BCRdf[name] = np.nan
    fh = open(files)

    for lines in fh:
        lines = lines.split(',')
        #begin filling in TCR data frame
        if linecount == 5:
            TCRdf[name]['TCR total reads'] = lines[1]
            TCRdf[name]['TCR %reads w/linker'] = lines[2]
            TCRdf[name]['TCR %reads to CL'] = lines[3]
        #TCR alpha
        elif linecount == 8:
            TCRdf[name]['TCRa %read2 aligned'] = lines[2]
            TCRdf[name]['TCRa %read CDR3'] = lines[3]
            TCRdf[name]['TCRa %read w/constant'] = lines[4]
            TCRdf[name]['TCRa %aligned w/ cellular reads'] = lines[5]
            tcracellreads = TCRdf[name]['TCRa %aligned w/ cellular reads'] / 100
            tcraReads = int(lines[1])

        elif linecount == 11:
            TCRdf[name]['TCRa Total molecules'] = lines[1]
            TCRdf[name]['TCRa RSEC Depth'] = lines[4]
            TCRdf[name]['TCRa DBEC Depth'] = lines[7]
            TCRdf[name]['%TCRa RSEC putative reads'] = lines[14]
        #TCR beta
        elif linecount == 18:
            TCRdf[name]['TCRb %read2 aligned'] = lines[2]
            TCRdf[name]['TCR %read2 sum'] = TCRdf[name]['TCRb %read2 aligned'] + TCRdf[name]['TCRa %read2 aligned']
            TCRdf[name]['TCRb %read CDR3'] = lines[3]
            TCRdf[name]['TCRb %read w/constant'] = lines[4]
            TCRdf[name]['TCRb %aligned w/ cellular reads'] = lines[5]
            tcrbcellreads = TCRdf[name]['TCRb %aligned w/ cellular reads'] / 100
            tcrbReads = int(lines[1])

        elif linecount == 21:
            TCRdf[name]['TCRb Total molecules'] = lines[1]
            TCRdf[name]['TCRb RSEC Depth'] = lines[4]
            TCRdf[name]['TCRb DBEC Depth'] = lines[7]
            TCRdf[name]['%TCRb RSEC putative reads'] = lines[14]
            TCRdf[name]['TCR %useful reads'] = (((tcraReads * tcracellreads) + (tcrbReads * tcrbcellreads)) /
                                                TCRdf[name]['TCR total reads']) * 100
        #End of editing TCR dataframe
        #Begin editing BCR dataframe
        elif linecount == 25:
            BCRdf[name]['BCR total reads'] = lines[1]
            BCRdf[name]['BCR %reads w/linker'] = lines[2]
            BCRdf[name]['BCR %reads to CL'] = lines[3]

        #IgH Chain
        elif linecount == 28:
            BCRdf[name]['IgH %read2 aligned'] = lines[2]
            BCRdf[name]['IgH %read CDR3'] = lines[3]
            BCRdf[name]['IgH %aligned w/ cellular reads'] = lines[5]
            BCRdf[name]['IgH %read w/constant'] = lines[4]
            ighcellreads = BCRdf[name]['IgH %aligned w/ cellular reads'] / 100
            ighReads = int(lines[1])

        elif linecount == 31:
            BCRdf[name]['%IgH RSEC putative reads'] = lines[14]
            BCRdf[name]['IgH Total molecules'] = lines[1]
            BCRdf[name]['IgH RSEC Depth'] = lines[4]
            BCRdf[name]['IgH DBEC Depth'] = lines[7]

        #IgL Chain
        elif linecount == 38:
            BCRdf[name]['IgL %read2 aligned'] = lines[2]
            BCRdf[name]['BCR %read2 sum'] = BCRdf[name]['IgL %read2 aligned'] + BCRdf[name]['IgH %read2 aligned']
            BCRdf[name]['IgL %read CDR3'] = lines[3]
            BCRdf[name]['IgL %aligned w/ cellular reads'] = lines[5]
            BCRdf[name]['IgL %read w/constant'] = lines[4]
            iglcellreads = BCRdf[name]['IgL %aligned w/ cellular reads'] / 100
            iglReads = int(lines[1])

        elif linecount == 41:
            BCRdf[name]['%IgL RSEC putative reads'] = lines[14]
            BCRdf[name]['IgL Total molecules'] = lines[1]
            BCRdf[name]['IgL RSEC Depth'] = lines[4]
            BCRdf[name]['IgL DBEC Depth'] = lines[7]
            BCRdf[name]['BCR %useful reads'] = (((ighReads * ighcellreads) + (iglReads * iglcellreads)) /
                                                BCRdf[name]['BCR total reads']) * 100
        #End of editing BCR dataframe
        linecount += 1
    linecount = 0
    fh.close()

OutputFile = input('name of output: ')
OutputFilePath = PurePath(path).joinpath(OutputFile)
Combdf = pd.concat([TCRdf, BCRdf], axis=0)
Combdf.to_csv(OutputFilePath, index=True)
