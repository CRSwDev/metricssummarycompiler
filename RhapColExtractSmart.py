'''
Author: Imteaz Siddique
Scope: Enhanced information extracting script that can determine more effectively which library the data came from
'''

import csv
import pandas as pd
import numpy as np
from pathlib import Path
import argparse


def smartparse(metricpath, cells, output):
    header_section = ['#Sequencing Quality#', '#Library Quality#', '#Reads and Molecules#', '#Cells RSEC#']
    dfIndex = ['Total_Reads_in_FASTQ', 'Pct_Reads_Filtered_Out', 'Mean_Raw_Sequencing_Depth', 'Pct_Q30_Bases_in_Filtered_R2',
               'Pct_Assigned_to_Cell_Labels', 'Pct_Cellular_Reads_Aligned_Uniquely_to_Amplicons',
               'Mean_RSEC_Sequencing_Depth', 'Sequencing_Saturation', 'Putative_Cell_Count', 'cells loaded',
               'Pct_Reads_from_Putative_Cells', 'Mean_Reads_per_Cell', 'Mean_Molecules_per_Cell',
               'Mean_Targets_per_Cell', 'Reads_Cellular_Aligned_to_VDJ',
               'Pct_Reads_CDR3_Valid_Corrected_from_Putative_Cells', 'Mean_Reads_CDR3_Valid_Corrected_per_Putative_Cell',
               'Mean_Molecules_Corrected_per_Putative_Cell']
    resultdf = pd.DataFrame(index=dfIndex)
    file_list = list(Path(metricpath).glob('*Metrics_Summary.csv'))
    for files in file_list:
        name = files.name
        name = name.rstrip('_Metrics_Summary.csv')
        resultdf[name] = np.nan
        resultdf[name]['cells loaded'] = cells
        fh = open(str(files), 'r')
        data = csv.reader(fh)
        for row in data:
            try:
                if row[0] in header_section:
                    header_name = data.__next__()
                    row_data = data.__next__()
                    while 'mrna' not in row_data[-1].lower():
                        row_data = data.__next__()
                    for combine in zip(header_name, row_data):
                        if combine[0] in dfIndex:
                            resultdf[name][combine[0]] = combine[1]
            except IndexError:
                continue
            if '#VDJ#' in row:
                header_name = data.__next__()
                row_data = data.__next__()
                for combine in zip(header_name, row_data):
                    if combine[0] in dfIndex:
                        resultdf[name][combine[0]] = combine[1]
    filePath = Path(metricpath).joinpath(output)
    resultdf.to_csv(str(filePath))


def main():
    args = argparse.ArgumentParser(description='Experiment for a smarter way of parsing metrics summary files')
    args.add_argument('-dir', type=Path, help='Path to stored files')
    args.add_argument('-cells', type=int, help='Number of cells in the experiment')
    args.add_argument('-output', type=str, help='Name of output csv file')
    smartparse(args.parse_args().dir, args.parse_args().cells, args.parse_args().output)


if __name__ == '__main__':
    main()
